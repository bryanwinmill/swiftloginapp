//
//  AddContactController.swift
//  SwiftLoginApp
//
//  Created by Bryan Winmill on 10/26/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import Foundation
import UIKit
import Parse

class AddContactController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var name: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.name.delegate = self
        
    }
    
    
    @IBAction func saveContact(_ sender: UIBarButtonItem) {
        
        var contactName = PFObject(className:"Contact")
        contactName["name"] = name.text
        contactName["email"] = PFUser.current()?["email"] as? String
        
        contactName.saveInBackground {
            (success: Bool, error: Error?) in
            if (success) {
                
            } else {
                error?.localizedDescription
            }
        }
        
        self.performSegue(withIdentifier: "saveToContacts", sender: nil)
    }
    
    @IBAction func signOutAdd(_ sender: UIButton) {
        
        PFUser.logOutInBackground { (error) in
            print(PFUser.current()?["email"] as? String )
        }
        
        self.performSegue(withIdentifier: "menuFromAdd", sender: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

//
//  ContactController.swift
//  SwiftLoginApp
//
//  Created by Bryan Winmill on 10/26/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import Foundation
import UIKit
import Parse

class ContactController: UIViewController, UITextFieldDelegate {
    
    var contact:PFObject!
    
    @IBOutlet var name: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.text = contact.object(forKey: "name") as? String
    }
    
    @IBAction func deleteContact(_ sender: UIBarButtonItem) {
        
        contact.deleteInBackground()
        
        self.performSegue(withIdentifier: "toAllContacts", sender: nil)
    }
    
    
    @IBAction func signOutDel(_ sender: UIButton) {
        
        PFUser.logOutInBackground { (error) in
            print(PFUser.current()?["email"] as? String )
        }
        
        self.performSegue(withIdentifier: "menuFromContact", sender: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

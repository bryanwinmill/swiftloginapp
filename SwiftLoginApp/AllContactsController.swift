//
//  AllContactsController.swift
//  SwiftLoginApp
//
//  Created by Bryan Winmill on 10/26/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import Foundation
import UIKit
import Parse

class AllContactsController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var contacts: [PFObject] = []
    
    @IBOutlet var contactsTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contactsTableView.dataSource = self
        contactsTableView.delegate = self
        loadNamesArray {
            self.update()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        update()
    }
    
    func update() {
        contactsTableView.reloadData()
    }
    
    func loadNamesArray(completion: @escaping () -> Void) {
        
        let query = PFQuery(className:"Contact")
        query.whereKey("email", equalTo: PFUser.current()?["email"] as? String)
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                //Success
                
                if let objects = objects {
                    for object in objects {
                        self.contacts.append(object)
                    }
                    completion()
                }
            } else {
                print("Error")
            }
            
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let contact = contacts[indexPath.row]
        cell.textLabel?.text = contact.object(forKey: "name") as! String
        
        return cell
    }
    
    var selectedIndex = 0
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "loadContact", sender: nil)
    }
    
    
    @IBAction func signOut(_ sender: UIButton) {
        
        PFUser.logOutInBackground { (error) in
            print(PFUser.current()?["email"] as? String )
        }
        
        self.performSegue(withIdentifier: "menuFromContacts", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "loadContact"{
            if let vc = segue.destination as? ContactController {
                vc.contact = contacts[selectedIndex]
            }
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

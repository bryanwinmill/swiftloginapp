//
//  ViewController.swift
//  SwiftLoginApp
//
//  Created by Bryan Winmill on 10/24/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var email: UITextField!
    @IBOutlet var password: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.email.delegate = self
        self.password.delegate = self
        
        /*Save object
                var gameScore = PFObject(className:"GameScore")
                gameScore["score"] = 1337
                gameScore["playerName"] = "Sean Plott"
                gameScore["cheatMode"] = false
                gameScore.saveInBackground {
                    (success: Bool, error: Error?) in
                    if (success) {
                        // The object has been saved.
                        print(success)
                    } else {
                        // There was a problem, check error.description
                    }
        }*/
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }
    
    @IBAction func loginButton(_ sender: UIButton) {
        
        
        [PFUser.logInWithUsername(inBackground: email.text!, password: password.text!, block: { (user, error) in
            if(user != nil) {
                
                self.performSegue(withIdentifier: "login", sender: nil)
            }
        })]
        
    }
    
    @IBAction func signUpButton(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "signUp", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


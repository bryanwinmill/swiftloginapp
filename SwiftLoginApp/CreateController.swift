//
//  CreateController.swift
//  SwiftLoginApp
//
//  Created by Bryan Winmill on 10/26/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import Foundation
import UIKit
import Parse

class CreateController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var email: UITextField!
    
    @IBOutlet var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.email.delegate = self
        self.password.delegate = self
        
    }
    
    @IBAction func createUser(_ sender: UIButton) {
        
        let user = PFUser()
        user.username = email.text
        user.password = password.text
        user.email = email.text
        
        user.signUpInBackground { (s, e) in
            if let e = e {
                _ = e._userInfo! as? NSString
            } else {
                
                [PFUser.logInWithUsername(inBackground: self.email.text!, password: self.password.text!, block: { (user, error) in
                    if(user != nil) {
                        
                    }
                })]
                
                self.performSegue(withIdentifier: "accountCreated", sender: nil)
            }
        }
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
